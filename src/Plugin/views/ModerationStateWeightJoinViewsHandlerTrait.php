<?php

namespace Drupal\views_moderation_state_weights\Plugin\views;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Views;

/**
 * Assist views handler plugins to join the moderation state weight table.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
trait ModerationStateWeightJoinViewsHandlerTrait {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The base table alias.
   *
   * @var string
   */
  protected $tableBaseAlias;

  /**
   * The content moderation state table alias.
   *
   * @var string
   */
  protected $tableContentModerationStateAlias;

  /**
   * Ensure that the base table is available.
   *
   * @return string
   *   The alias of the base table.
   */
  protected function ensureBaseTable() {
    if (!isset($this->tableBaseAlias)) {
      $this->tableBaseAlias = $this->query->ensureTable($this->table, $this->relationship);
    }

    return $this->tableBaseAlias;
  }

  /**
   * Ensure that the content moderation state table relationship is available.
   *
   * @param string $base_table_alias
   *   The alias of the base table from which to join the content moderation
   *   state table.
   *
   * @return string
   *   The alias of the content moderation state table.
   */
  protected function ensureContentModerationStateTable(string $base_table_alias) {
    if (!isset($this->tableContentModerationStateAlias)) {
      $base_entity_type = $this->getEntityTypeManager()->getDefinition($this->getEntityType());
      $content_moderation_state_entity_type = $this->getEntityTypeManager()->getDefinition('content_moderation_state');
      $join_plugin_manager = Views::pluginManager('join');

      $configuration = [
        'table' => $content_moderation_state_entity_type->getRevisionDataTable(),
        'field' => 'content_entity_revision_id',
        'left_table' => $base_table_alias,
        'left_field' => $base_entity_type->getKey('revision'),
        'extra' => [
          [
            'field' => 'content_entity_type_id',
            'value' => $base_entity_type->id(),
          ],
          [
            'field' => 'content_entity_id',
            'left_field' => $base_entity_type->getKey('id'),
          ],
        ],
      ];

      if ($base_entity_type->isTranslatable()) {
        $configuration['extra'][] = [
          'field' => $content_moderation_state_entity_type->getKey('langcode'),
          'left_field' => $base_entity_type->getKey('langcode'),
        ];
      }

      $join = $join_plugin_manager->createInstance('standard', $configuration);
      $this->tableContentModerationStateAlias = $this->query->addRelationship('content_moderation_state', $join, $content_moderation_state_entity_type->getRevisionDataTable());
    }

    return $this->tableContentModerationStateAlias;
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    if (!isset($this->tableAlias)) {
      $content_moderation_state_table_alias = $this->ensureContentModerationStateTable($this->ensureBaseTable());
      $join_plugin_manager = Views::pluginManager('join');

      $configuration = [
        'table' => 'views_moderation_state_weights',
        'field' => 'moderation_state',
        'left_table' => $content_moderation_state_table_alias,
        'left_field' => 'moderation_state',
        'extra' => [
          [
            'field' => 'workflow',
            'left_field' => 'workflow',
          ],
        ],
      ];

      $join = $join_plugin_manager->createInstance('standard', $configuration);
      $this->tableAlias = $this->query->addRelationship('views_moderation_state_weights', $join, 'views_moderation_state_weights');
    }

    return $this->tableAlias;
  }

  /**
   * Get the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

}
