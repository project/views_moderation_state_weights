<?php

namespace Drupal\views_moderation_state_weights;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles the moderation state weights table.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
class ModerationStateWeightHandler implements ContainerInjectionInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a ModerationStateWeightHandler object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * Delete the supplied workflow's state weights.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow for which to delete its state weights.
   */
  public function delete(WorkflowInterface $workflow): void {
    $query = $this->database->delete('views_moderation_state_weights');
    $query->condition('workflow', $workflow->id());
    $query->execute();
  }

  /**
   * Insert the supplied workflow's state weights.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow for which to delete its state weights.
   */
  public function insert(WorkflowInterface $workflow): void {
    $query = $this->database->insert('views_moderation_state_weights');

    $query->fields([
      'workflow',
      'moderation_state',
      'weight',
    ]);

    foreach ($workflow->getTypePlugin()->getStates() as $state) {
      $query->values([
        'workflow' => $workflow->id(),
        'moderation_state' => $state->id(),
        'weight' => $state->weight(),
      ]);
    }

    $query->execute();
  }

  /**
   * Updates the supplied workflow's state weights.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow for which to update its state weights.
   */
  public function update(WorkflowInterface $workflow): void {
    $transaction = $this->database->startTransaction('views_moderation_state_weights');

    if ($transaction) {
      $this->delete($workflow);
      $this->insert($workflow);
    }
  }

}
