<?php

namespace Drupal\Tests\views_moderation_state_weights\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\views_moderation_state_weights\ModerationStateWeightHandler;
use Drupal\workflows\StateInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Tests the moderation state weight handler service.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group views_moderation_state_weights
 */
class ModerationStateWeightHandlerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views_moderation_state_weights',
    'views_moderation_state_weights_test',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('views_moderation_state_weights', [
      'views_moderation_state_weights',
    ]);
  }

  /**
   * Create a workflow to use during tests.
   *
   * @return \Drupal\workflows\WorkflowInterface
   *   A workflow to use during tests.
   */
  protected function createWorkflow(): WorkflowInterface {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $workflow_storage = $entity_type_manager->getStorage('workflow');

    /** @var \Drupal\workflows\WorkflowInterface */
    $workflow = $workflow_storage->create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
      'type' => 'basic',
      'type_settings' => [
        'states' => [],
        'transitions' => [],
      ],
    ]);

    $desired_states = [
      \strtolower($this->randomMachineName()) => -2,
      \strtolower($this->randomMachineName()) => -1,
      \strtolower($this->randomMachineName()) => 0,
      \strtolower($this->randomMachineName()) => 1,
      \strtolower($this->randomMachineName()) => 2,
    ];

    foreach ($desired_states as $state => $weight) {
      $workflow->getTypePlugin()->addState($state, $state);
      $workflow->getTypePlugin()->setStateWeight($state, $weight);
    }

    $actual_states = $this->getStateWeights($workflow->getTypePlugin()->getStates());
    $this->assertSame($desired_states, $actual_states);

    return $workflow;
  }

  /**
   * Get the weight of each supplied state.
   *
   * @param \Drupal\workflows\StateInterface[] $states
   *   A list of states for which to retrieve their weight.
   *
   * @return int[]
   *   An associative array of weights keyed by state.
   */
  protected function getStateWeights(array $states) {
    $results = \array_map(function (StateInterface $state) {
      return \intval($state->weight());
    }, $states);

    \asort($results, \SORT_NUMERIC);
    return $results;
  }

  /**
   * Get state weights as stored in the moderation state weights table.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow for which to get state weights.
   *
   * @return int[]
   *   An associative array of moderation state weights keyed by state.
   */
  protected function getStateWeightsForWorkflow(WorkflowInterface $workflow) {
    /** @var \Drupal\Core\Database\Connection */
    $connection = $this->container->get('database');

    // Create a query to fetch data from the moderation state weights table.
    $query = $connection->select('views_moderation_state_weights', 'v');

    // Fetch the moderation state and weight columns.
    $query->addField('v', 'moderation_state', 'moderation_state');
    $query->addField('v', 'weight', 'weight');

    // Only fetch results for the supplied workflow.
    $query->condition('workflow', $workflow->id());

    $results = \array_map(\intval(...), $query->execute()->fetchAllKeyed());
    \asort($results, \SORT_NUMERIC);

    return $results;
  }

  /**
   * Tests the moderation state weight handler on workflow delete.
   */
  public function testModerationStateWeightHandlerDelete() {
    /** @var \Drupal\views_moderation_state_weights\ModerationStateWeightHandler */
    $moderation_state_weight_handler = \Drupal::classResolver(ModerationStateWeightHandler::class);
    $moderation_state_weight_handler->insert($workflow = $this->createWorkflow());
    $moderation_state_weight_handler->delete($workflow);

    $actual_weights = $this->getStateWeightsForWorkflow($workflow);
    $this->assertCount(0, $actual_weights, 'State weights table has no entries for workflow after delete');
  }

  /**
   * Tests the moderation state weight handler on workflow insert.
   */
  public function testModerationStateWeightHandlerInsert() {
    /** @var \Drupal\views_moderation_state_weights\ModerationStateWeightHandler */
    $moderation_state_weight_handler = \Drupal::classResolver(ModerationStateWeightHandler::class);
    $moderation_state_weight_handler->insert($workflow = $this->createWorkflow());

    $expected_weights = $this->getStateWeights($workflow->getTypePlugin()->getStates());
    $actual_weights = $this->getStateWeightsForWorkflow($workflow);
    $this->assertSame($expected_weights, $actual_weights, 'State weights table matches workflow after insert');
  }

  /**
   * Tests the moderation state weight handler on workflow update.
   */
  public function testModerationStateWeightHandlerUpdate() {
    /** @var \Drupal\views_moderation_state_weights\ModerationStateWeightHandler */
    $moderation_state_weight_handler = \Drupal::classResolver(ModerationStateWeightHandler::class);
    $moderation_state_weight_handler->insert($workflow = $this->createWorkflow());
    $workflow->getTypePlugin()->addState($new_state = \strtolower($this->randomMachineName()), $new_state);
    $moderation_state_weight_handler->update($workflow);

    $expected_weights = $this->getStateWeights($workflow->getTypePlugin()->getStates());
    $actual_weights = $this->getStateWeightsForWorkflow($workflow);
    $this->assertArrayHasKey($new_state, $expected_weights);
    $this->assertSame($expected_weights, $actual_weights, 'State weights table matches workflow after update');
  }

}
