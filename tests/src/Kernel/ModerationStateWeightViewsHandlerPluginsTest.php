<?php

namespace Drupal\Tests\views_moderation_state_weights\Kernel;

use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Views;

/**
 * Tests the Views handler plugins provided by this module.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group views_moderation_state_weights
 */
class ModerationStateWeightViewsHandlerPluginsTest extends ViewsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'views',
    'views_moderation_state_weights',
    'views_moderation_state_weights_test',
    'workflows',
  ];

  /**
   * The workflow being used under test.
   *
   * @var \Drupal\workflows\WorkflowInterface
   */
  protected $workflow;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('entity_test_no_bundle');

    $this->installSchema('views_moderation_state_weights', [
      'views_moderation_state_weights',
    ]);

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $workflow_storage = $entity_type_manager->getStorage('workflow');

    /** @var \Drupal\workflows\WorkflowInterface */
    $this->workflow = $workflow_storage->create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
      'type' => 'content_moderation',
      'type_settings' => [
        'entity_types' => [
          'entity_test_no_bundle' => [
            'entity_test_no_bundle',
          ],
        ],
      ],
    ]);

    $this->workflow->save();
  }

  /**
   * Data provider for ::testViewsPlugins().
   */
  public function providerTestViewsPlugins() {
    return [
      'ascending sort, ascending states' => [
        [
          'draft' => 0,
          'published' => 1,
        ],
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
          ],
        ],
        TRUE,
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
            'moderation_state_weight' => 1,
          ],
        ],
      ],
      'descending sort, ascending states' => [
        [
          'draft' => 0,
          'published' => 1,
        ],
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
          ],
        ],
        FALSE,
        [
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 0,
          ],
        ],
      ],
      'ascending sort, descending states' => [
        [
          'draft' => 1,
          'published' => 0,
        ],
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
          ],
        ],
        TRUE,
        [
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 1,
          ],
        ],
      ],
      'descending sort, descending states' => [
        [
          'draft' => 1,
          'published' => 0,
        ],
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
          ],
        ],
        FALSE,
        [
          [
            'name' => 'test entity #0',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #2',
            'moderation_state' => 'draft',
            'moderation_state_weight' => 1,
          ],
          [
            'name' => 'test entity #1',
            'moderation_state' => 'published',
            'moderation_state_weight' => 0,
          ],
          [
            'name' => 'test entity #3',
            'moderation_state' => 'published',
            'moderation_state_weight' => 0,
          ],
        ],
      ],
    ];
  }

  /**
   * Tests the Views handler plugins provided by this module.
   *
   * @dataProvider providerTestViewsPlugins
   */
  public function testViewsPlugins(array $states, array $entities, bool $sort_ascending, array $expected_result) {
    $this->installConfig('views_moderation_state_weights_test');

    $view = Views::getView('views_moderation_state_weights_test_view');
    $view->setDisplay();

    $workflow_type = $this->workflow->getTypePlugin();
    foreach ($states as $state => $weight) {
      $this->assertTrue($workflow_type->hasState($state));
      $workflow_type->setStateWeight($state, $weight);
    }

    $this->workflow->save();

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $storage = $entity_type_manager->getStorage('entity_test_no_bundle');

    foreach ($entities as $data) {
      $entity = $storage->create($data);
      $entity->save();
    }

    if (!$sort_ascending) {
      /** @var \Drupal\views\Plugin\Views\sort\SortPluginBase */
      $sort_handler = $view->getDisplay()->getHandler('sort', 'moderation_state_weight');
      $sort_handler->options['order'] = 'DESC';
    }

    // We must execute the view prior to referencing a handler's alias.
    $this->executeView($view);

    /** @var \Drupal\views\Plugin\Views\field\FieldPluginBase */
    $field_handler = $view->getDisplay()->getHandler('field', 'moderation_state_weight');

    // Define the column map between each view row and its expected values.
    $column_map = [
      'name' => 'name',
      'moderation_state' => 'moderation_state',
      $field_handler->field_alias => 'moderation_state_weight',
    ];

    $this->assertCount(\count($entities), $view->result, 'The result count matches the entity count');
    $this->assertIdenticalResultSet($view, $expected_result, $column_map, 'The actual view results match the expected results');
  }

}
