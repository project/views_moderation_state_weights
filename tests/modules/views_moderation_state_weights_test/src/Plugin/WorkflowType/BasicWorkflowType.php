<?php

namespace Drupal\views_moderation_state_weights_test\Plugin\WorkflowType;

use Drupal\workflows\Plugin\WorkflowTypeBase;

/**
 * Basic workflow type for testing.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @WorkflowType(
 *   id = "basic",
 *   label = @Translation("Basic Workflow"),
 * )
 */
class BasicWorkflowType extends WorkflowTypeBase {

}
