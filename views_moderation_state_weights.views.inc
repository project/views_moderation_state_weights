<?php

/**
 * @file
 * Provides views data for this module.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/**
 * Implements hook_views_data().
 *
 * The content moderation module already takes care of invalidating Views data
 * when an applicable workflow is updated.
 *
 * @see \content_moderation_workflow_insert()
 * @see \content_moderation_workflow_update()
 */
function views_moderation_state_weights_views_data() {
  $data = [];

  /** @var \Drupal\content_moderation\ModerationInformationInterface */
  $moderation_information = \Drupal::service('content_moderation.moderation_information');
  $entity_type_manager = \Drupal::entityTypeManager();

  foreach ($entity_type_manager->getDefinitions() as $entity_type) {
    if (!$moderation_information->isModeratedEntityType($entity_type)) {
      continue;
    }

    $table = $entity_type->getDataTable() ?: $entity_type->getBaseTable();
    $data[$table]['moderation_state_weight'] = [
      'title' => t('Moderation state weight'),
      'field' => [
        'id' => 'moderation_state_weight_field',
        'field' => 'weight',
      ],
      'sort' => [
        'id' => 'moderation_state_weight_sort',
        'field' => 'weight',
      ],
    ];

    $revision_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
    $data[$revision_table]['moderation_state_weight'] = [
      'title' => t('Moderation state weight'),
      'field' => [
        'id' => 'moderation_state_weight_field',
        'field' => 'weight',
      ],
      'sort' => [
        'id' => 'moderation_state_weight_sort',
        'field' => 'weight',
      ],
    ];
  }

  return $data;
}
